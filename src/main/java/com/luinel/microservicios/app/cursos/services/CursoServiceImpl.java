package com.luinel.microservicios.app.cursos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.microservicios.app.cursos.clients.AlumnoFeignClient;
import com.luinel.microservicios.app.cursos.clients.RespuestaFeignClient;
import com.luinel.microservicios.app.cursos.models.entity.Curso;
import com.luinel.microservicios.app.cursos.models.repository.CursoRepository;
import com.luinel.microservicios.commons.alumnos.models.entity.Alumno;
import com.luinel.microservicios.commons.services.CommonServiceImpl;

@Service
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursoService {

	@Autowired
	private RespuestaFeignClient clientRespuesta;
	
	@Autowired
	private AlumnoFeignClient clientAlumno;
	
	@Override
	@Transactional(readOnly = true)
	public Curso findCursoByAlumnoId(Long AlumnoId) {
		return repository.findCursoByAlumnoId(AlumnoId);
	}

	@Override
	public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId) {
		return clientRespuesta.obtenerExamenesIdsConRespuestasAlumno(alumnoId);
	}

	@Override
	public Iterable<Alumno> obtenerAlumnosPorCurso(Iterable<Long> ids) {
		return clientAlumno.obtenerAlumnosPorCurso(ids);
	}

	@Override
	@Transactional
	public void eliminarCursoAlumnoPorId(Long alumnoId) {
		repository.eliminarCursoAlumnoPorId(alumnoId);
	}
	
}
