package com.luinel.microservicios.app.cursos.services;

import com.luinel.microservicios.app.cursos.models.entity.Curso;
import com.luinel.microservicios.commons.alumnos.models.entity.Alumno;
import com.luinel.microservicios.commons.services.CommonService;

public interface CursoService extends CommonService<Curso> {
	
	public Curso findCursoByAlumnoId(Long AlumnoId);
	
	public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId);
	
	public Iterable<Alumno> obtenerAlumnosPorCurso(Iterable<Long> ids);
	
	public void eliminarCursoAlumnoPorId(Long alumnoId);
}
